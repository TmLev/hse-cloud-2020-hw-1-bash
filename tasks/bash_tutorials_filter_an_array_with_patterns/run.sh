#!/usr/bin/env bash

readarray -t COUNTRIES

FILTERED=(${COUNTRIES[@]/*[aA]*/})

echo "${FILTERED[@]}"

