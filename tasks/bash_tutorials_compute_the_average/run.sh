#!/usr/bin/env bash

read N
readarray -t NUMBERS

SUM=$(echo "${NUMBERS[@]/%/+} 0" | bc -l)
AVERAGE=$(echo "${SUM} / ${N}" | bc -l)

printf "%.3f\n" "${AVERAGE}"

