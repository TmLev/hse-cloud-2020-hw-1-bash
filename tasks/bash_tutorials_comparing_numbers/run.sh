#!/usr/bin/env bash

read X
read Y

if [ "$X" -lt "$Y" ]; then
  printf "X is less than Y"
elif [ "$X" -gt "$Y" ]; then
  printf "X is greater than Y"
else
  printf "X is equal to Y"
fi

