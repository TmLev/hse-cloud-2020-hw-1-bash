#!/usr/bin/env bash

readarray -t SIDES

UNIQUE=($(printf "%s\n" "${SIDES[@]}" | sort -u))
LEN=${#UNIQUE[@]}

if [ "$LEN" -eq 1 ]; then
  printf "EQUILATERAL"
elif [ "$LEN" -eq 2 ]; then
  printf "ISOSCELES"
else
  printf "SCALENE"
fi

