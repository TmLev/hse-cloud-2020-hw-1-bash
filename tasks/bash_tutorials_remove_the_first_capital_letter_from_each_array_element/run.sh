#!/usr/bin/env bash

readarray -t COUNTRIES

COUNTRIES=( ${COUNTRIES[@]/[A-Z]/.} )

echo "${COUNTRIES[@]}"

