#!/usr/bin/env bash

read EXPRESSION
RESULT=$(echo "${EXPRESSION}" | bc -l)
printf %.3f "${RESULT}"

