#!/usr/bin/env bash

read CHAR

CHAR=${CHAR,,}

if [ "$CHAR" = "y" ]; then
  printf "YES"
elif [ "$CHAR" = "n" ]; then
  printf "NO"
fi

